<?php

namespace App\Repository;

use App\Entity\Tprestation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tprestation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tprestation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tprestation[]    findAll()
 * @method Tprestation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TprestationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tprestation::class);
    }

    // /**
    //  * @return Tprestation[] Returns an array of Tprestation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tprestation
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
