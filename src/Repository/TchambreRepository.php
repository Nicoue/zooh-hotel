<?php

namespace App\Repository;

use App\Entity\Tchambre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tchambre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tchambre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tchambre[]    findAll()
 * @method Tchambre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TchambreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tchambre::class);
    }

    // /**
    //  * @return Tchambre[] Returns an array of Tchambre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tchambre
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
