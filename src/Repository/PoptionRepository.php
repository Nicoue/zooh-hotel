<?php

namespace App\Repository;

use App\Entity\Poption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Poption|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poption|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poption[]    findAll()
 * @method Poption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Poption::class);
    }

    // /**
    //  * @return Poption[] Returns an array of Poption objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Poption
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
