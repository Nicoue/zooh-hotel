<?php

namespace App\Repository;

use App\Entity\Tcategorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tcategorie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tcategorie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tcategorie[]    findAll()
 * @method Tcategorie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TcategorieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tcategorie::class);
    }

    // /**
    //  * @return Tcategorie[] Returns an array of Tcategorie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tcategorie
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
