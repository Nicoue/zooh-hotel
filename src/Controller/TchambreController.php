<?php

namespace App\Controller;

use App\Entity\Tchambre;
use App\Form\TchambreType;
use App\Repository\TchambreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tchambre")
 */
class TchambreController extends AbstractController
{
    /**
     * @Route("/", name="tchambre_index", methods={"GET"})
     */
    public function index(TchambreRepository $tchambreRepository): Response
    {
        return $this->render('tchambre/index.html.twig', [
            'tchambres' => $tchambreRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tchambre_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tchambre = new Tchambre();
        $form = $this->createForm(TchambreType::class, $tchambre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tchambre);
            $entityManager->flush();

            return $this->redirectToRoute('tchambre_index');
        }

        return $this->render('tchambre/new.html.twig', [
            'tchambre' => $tchambre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tchambre_show", methods={"GET"})
     */
    public function show(Tchambre $tchambre): Response
    {
        return $this->render('tchambre/show.html.twig', [
            'tchambre' => $tchambre,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tchambre_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tchambre $tchambre): Response
    {
        $form = $this->createForm(TchambreType::class, $tchambre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tchambre_index');
        }

        return $this->render('tchambre/edit.html.twig', [
            'tchambre' => $tchambre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tchambre_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tchambre $tchambre): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tchambre->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tchambre);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tchambre_index');
    }
}
