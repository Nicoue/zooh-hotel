<?php

namespace App\Controller;

use App\Entity\Ptypesociete;
use App\Form\PtypesocieteType;
use App\Repository\PtypesocieteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ptypesociete")
 */
class PtypesocieteController extends AbstractController
{
    /**
     * @Route("/", name="ptypesociete_index", methods={"GET"})
     */
    public function index(PtypesocieteRepository $ptypesocieteRepository): Response
    {
        return $this->render('ptypesociete/index.html.twig', [
            'ptypesocietes' => $ptypesocieteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ptypesociete_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $ptypesociete = new Ptypesociete();
        $form = $this->createForm(PtypesocieteType::class, $ptypesociete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ptypesociete);
            $entityManager->flush();

            return $this->redirectToRoute('ptypesociete_index');
        }

        return $this->render('ptypesociete/new.html.twig', [
            'ptypesociete' => $ptypesociete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ptypesociete_show", methods={"GET"})
     */
    public function show(Ptypesociete $ptypesociete): Response
    {
        return $this->render('ptypesociete/show.html.twig', [
            'ptypesociete' => $ptypesociete,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ptypesociete_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Ptypesociete $ptypesociete): Response
    {
        $form = $this->createForm(PtypesocieteType::class, $ptypesociete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ptypesociete_index');
        }

        return $this->render('ptypesociete/edit.html.twig', [
            'ptypesociete' => $ptypesociete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ptypesociete_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Ptypesociete $ptypesociete): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ptypesociete->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ptypesociete);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ptypesociete_index');
    }
}
