<?php

namespace App\Controller;

use App\Entity\Psociete;
use App\Form\PsocieteType;
use App\Repository\PsocieteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/psociete")
 */
class PsocieteController extends AbstractController
{
    /**
     * @Route("/", name="psociete_index", methods={"GET"})
     */
    public function index(PsocieteRepository $psocieteRepository): Response
    {
        return $this->render('psociete/index.html.twig', [
            'psocietes' => $psocieteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="psociete_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $psociete = new Psociete();
        $form = $this->createForm(PsocieteType::class, $psociete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($psociete);
            $entityManager->flush();

            return $this->redirectToRoute('psociete_index');
        }

        return $this->render('psociete/new.html.twig', [
            'psociete' => $psociete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="psociete_show", methods={"GET"})
     */
    public function show(Psociete $psociete): Response
    {
        return $this->render('psociete/show.html.twig', [
            'psociete' => $psociete,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="psociete_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Psociete $psociete): Response
    {
        $form = $this->createForm(PsocieteType::class, $psociete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('psociete_index');
        }

        return $this->render('psociete/edit.html.twig', [
            'psociete' => $psociete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="psociete_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Psociete $psociete): Response
    {
        if ($this->isCsrfTokenValid('delete'.$psociete->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($psociete);
            $entityManager->flush();
        }

        return $this->redirectToRoute('psociete_index');
    }
}
