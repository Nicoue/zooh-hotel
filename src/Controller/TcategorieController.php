<?php

namespace App\Controller;

use App\Entity\Tcategorie;
use App\Form\TcategorieType;
use App\Repository\TcategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tcategorie")
 */
class TcategorieController extends AbstractController
{
    /**
     * @Route("/", name="tcategorie_index", methods={"GET"})
     */
    public function index(TcategorieRepository $tcategorieRepository): Response
    {
        return $this->render('tcategorie/index.html.twig', [
            'tcategories' => $tcategorieRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tcategorie_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tcategorie = new Tcategorie();
        $form = $this->createForm(TcategorieType::class, $tcategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tcategorie);
            $entityManager->flush();

            return $this->redirectToRoute('tcategorie_index');
        }

        return $this->render('tcategorie/new.html.twig', [
            'tcategorie' => $tcategorie,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tcategorie_show", methods={"GET"})
     */
    public function show(Tcategorie $tcategorie): Response
    {
        return $this->render('tcategorie/show.html.twig', [
            'tcategorie' => $tcategorie,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tcategorie_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tcategorie $tcategorie): Response
    {
        $form = $this->createForm(TcategorieType::class, $tcategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tcategorie_index');
        }

        return $this->render('tcategorie/edit.html.twig', [
            'tcategorie' => $tcategorie,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tcategorie_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tcategorie $tcategorie): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tcategorie->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tcategorie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tcategorie_index');
    }
}
