<?php

namespace App\Controller;

use App\Entity\Poption;
use App\Form\PoptionType;
use App\Repository\PoptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/poption")
 */
class PoptionController extends AbstractController
{
    /**
     * @Route("/", name="poption_index", methods={"GET"})
     */
    public function index(PoptionRepository $poptionRepository): Response
    {
        return $this->render('poption/index.html.twig', [
            'poptions' => $poptionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="poption_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $poption = new Poption();
        $form = $this->createForm(PoptionType::class, $poption);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($poption);
            $entityManager->flush();

            return $this->redirectToRoute('poption_index');
        }

        return $this->render('poption/new.html.twig', [
            'poption' => $poption,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="poption_show", methods={"GET"})
     */
    public function show(Poption $poption): Response
    {
        return $this->render('poption/show.html.twig', [
            'poption' => $poption,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="poption_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Poption $poption): Response
    {
        $form = $this->createForm(PoptionType::class, $poption);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('poption_index');
        }

        return $this->render('poption/edit.html.twig', [
            'poption' => $poption,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="poption_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Poption $poption): Response
    {
        if ($this->isCsrfTokenValid('delete'.$poption->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($poption);
            $entityManager->flush();
        }

        return $this->redirectToRoute('poption_index');
    }
}
