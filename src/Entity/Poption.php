<?php

namespace App\Entity;

use App\Repository\PoptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PoptionRepository::class)
 */
class Poption
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @ORM\ManyToMany(targetEntity=Tchambre::class, mappedBy="poptions")
     */
    private $tchambres;

    public function __construct()
    {
        $this->tchambres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * @return Collection|Tchambre[]
     */
    public function getTchambres(): Collection
    {
        return $this->tchambres;
    }

    public function addTchambre(Tchambre $tchambre): self
    {
        if (!$this->tchambres->contains($tchambre)) {
            $this->tchambres[] = $tchambre;
            $tchambre->addPoption($this);
        }

        return $this;
    }

    public function removeTchambre(Tchambre $tchambre): self
    {
        if ($this->tchambres->removeElement($tchambre)) {
            $tchambre->removePoption($this);
        }

        return $this;
    }
}
