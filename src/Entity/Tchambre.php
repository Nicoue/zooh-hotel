<?php

namespace App\Entity;

use App\Repository\TchambreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TchambreRepository::class)
 */
class Tchambre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numchambre;

    /**
     * @ORM\ManyToOne(targetEntity=Tcategorie::class, inversedBy="tchambres")
     */
    private $tcategories;

    /**
     * @ORM\ManyToMany(targetEntity=Poption::class, inversedBy="tchambres")
     */
    private $poptions;

    /**
     * @ORM\OneToMany(targetEntity=Preduction::class, mappedBy="tchambres")
     */
    private $preductions;

    public function __construct()
    {
        $this->poptions = new ArrayCollection();
        $this->preductions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumchambre(): ?string
    {
        return $this->numchambre;
    }

    public function setNumchambre(?string $numchambre): self
    {
        $this->numchambre = $numchambre;

        return $this;
    }

    public function getTcategories(): ?Tcategorie
    {
        return $this->tcategories;
    }

    public function setTcategories(?Tcategorie $tcategories): self
    {
        $this->tcategories = $tcategories;

        return $this;
    }

    /**
     * @return Collection|Poption[]
     */
    public function getPoptions(): Collection
    {
        return $this->poptions;
    }

    public function addPoption(Poption $poption): self
    {
        if (!$this->poptions->contains($poption)) {
            $this->poptions[] = $poption;
        }

        return $this;
    }

    public function removePoption(Poption $poption): self
    {
        $this->poptions->removeElement($poption);

        return $this;
    }

    /**
     * @return Collection|Preduction[]
     */
    public function getPreductions(): Collection
    {
        return $this->preductions;
    }

    public function addPreduction(Preduction $preduction): self
    {
        if (!$this->preductions->contains($preduction)) {
            $this->preductions[] = $preduction;
            $preduction->setTchambres($this);
        }

        return $this;
    }

    public function removePreduction(Preduction $preduction): self
    {
        if ($this->preductions->removeElement($preduction)) {
            // set the owning side to null (unless already changed)
            if ($preduction->getTchambres() === $this) {
                $preduction->setTchambres(null);
            }
        }

        return $this;
    }
}
