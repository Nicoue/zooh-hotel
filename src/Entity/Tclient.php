<?php

namespace App\Entity;

use App\Repository\TclientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TclientRepository::class)
 */
class Tclient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numpasseport;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datexp;

    /**
     * @ORM\OneToMany(targetEntity=Treservation::class, mappedBy="tclients")
     */
    private $treservations;

    /**
     * @ORM\OneToMany(targetEntity=Preduction::class, mappedBy="tclients")
     */
    private $preductions;

    public function __construct()
    {
        $this->treservations = new ArrayCollection();
        $this->preductions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getNumpasseport(): ?string
    {
        return $this->numpasseport;
    }

    public function setNumpasseport(?string $numpasseport): self
    {
        $this->numpasseport = $numpasseport;

        return $this;
    }

    public function getDatexp(): ?\DateTimeInterface
    {
        return $this->datexp;
    }

    public function setDatexp(?\DateTimeInterface $datexp): self
    {
        $this->datexp = $datexp;

        return $this;
    }

    /**
     * @return Collection|Treservation[]
     */
    public function getTreservations(): Collection
    {
        return $this->treservations;
    }

    public function addTreservation(Treservation $treservation): self
    {
        if (!$this->treservations->contains($treservation)) {
            $this->treservations[] = $treservation;
            $treservation->setTclients($this);
        }

        return $this;
    }

    public function removeTreservation(Treservation $treservation): self
    {
        if ($this->treservations->removeElement($treservation)) {
            // set the owning side to null (unless already changed)
            if ($treservation->getTclients() === $this) {
                $treservation->setTclients(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Preduction[]
     */
    public function getPreductions(): Collection
    {
        return $this->preductions;
    }

    public function addPreduction(Preduction $preduction): self
    {
        if (!$this->preductions->contains($preduction)) {
            $this->preductions[] = $preduction;
            $preduction->setTclients($this);
        }

        return $this;
    }

    public function removePreduction(Preduction $preduction): self
    {
        if ($this->preductions->removeElement($preduction)) {
            // set the owning side to null (unless already changed)
            if ($preduction->getTclients() === $this) {
                $preduction->setTclients(null);
            }
        }

        return $this;
    }
}
