<?php

namespace App\Entity;

use App\Repository\TcategorieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TcategorieRepository::class)
 */
class Tcategorie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity=Tchambre::class, mappedBy="tcategories")
     */
    private $tchambres;

    public function __construct()
    {
        $this->tchambres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|Tchambre[]
     */
    public function getTchambres(): Collection
    {
        return $this->tchambres;
    }

    public function addTchambre(Tchambre $tchambre): self
    {
        if (!$this->tchambres->contains($tchambre)) {
            $this->tchambres[] = $tchambre;
            $tchambre->setTcategories($this);
        }

        return $this;
    }

    public function removeTchambre(Tchambre $tchambre): self
    {
        if ($this->tchambres->removeElement($tchambre)) {
            // set the owning side to null (unless already changed)
            if ($tchambre->getTcategories() === $this) {
                $tchambre->setTcategories(null);
            }
        }

        return $this;
    }
}
