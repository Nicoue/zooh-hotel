<?php

namespace App\Entity;

use App\Repository\PsocieteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PsocieteRepository::class)
 */
class Psociete
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomsocite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ifu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mcf;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rccm;

    /**
     * @ORM\ManyToOne(targetEntity=Ptypesociete::class, inversedBy="psocietes")
     */
    private $ptypesocietes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomsocite(): ?string
    {
        return $this->nomsocite;
    }

    public function setNomsocite(?string $nomsocite): self
    {
        $this->nomsocite = $nomsocite;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getIfu(): ?int
    {
        return $this->ifu;
    }

    public function setIfu(?int $ifu): self
    {
        $this->ifu = $ifu;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getMcf(): ?string
    {
        return $this->mcf;
    }

    public function setMcf(?string $mcf): self
    {
        $this->mcf = $mcf;

        return $this;
    }

    public function getRccm(): ?string
    {
        return $this->rccm;
    }

    public function setRccm(?string $rccm): self
    {
        $this->rccm = $rccm;

        return $this;
    }

    public function getPtypesocietes(): ?Ptypesociete
    {
        return $this->ptypesocietes;
    }

    public function setPtypesocietes(?Ptypesociete $ptypesocietes): self
    {
        $this->ptypesocietes = $ptypesocietes;

        return $this;
    }
}
