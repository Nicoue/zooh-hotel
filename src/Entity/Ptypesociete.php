<?php

namespace App\Entity;

use App\Repository\PtypesocieteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PtypesocieteRepository::class)
 */
class Ptypesociete
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @ORM\OneToMany(targetEntity=Psociete::class, mappedBy="ptypesocietes")
     */
    private $psocietes;

    public function __construct()
    {
        $this->psocietes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * @return Collection|Psociete[]
     */
    public function getPsocietes(): Collection
    {
        return $this->psocietes;
    }

    public function addPsociete(Psociete $psociete): self
    {
        if (!$this->psocietes->contains($psociete)) {
            $this->psocietes[] = $psociete;
            $psociete->setPtypesocietes($this);
        }

        return $this;
    }

    public function removePsociete(Psociete $psociete): self
    {
        if ($this->psocietes->removeElement($psociete)) {
            // set the owning side to null (unless already changed)
            if ($psociete->getPtypesocietes() === $this) {
                $psociete->setPtypesocietes(null);
            }
        }

        return $this;
    }
}
