<?php

namespace App\Entity;

use App\Repository\TreservationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TreservationRepository::class)
 */
class Treservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datedebut;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $datefin;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $montantarh;

    /**
     * @ORM\Column(type="boolean")
     */
    private $statut  = false;

    /**
     * @ORM\ManyToOne(targetEntity=Tclient::class, inversedBy="treservations")
     */
    private $tclients;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(?\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(?\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getMontantarh(): ?int
    {
        return $this->montantarh;
    }

    public function setMontantarh(?int $montantarh): self
    {
        $this->montantarh = $montantarh;

        return $this;
    }

    public function getStatut(): ?bool
    {
        return $this->statut;
    }

    public function setStatut(?bool $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getTclients(): ?Tclient
    {
        return $this->tclients;
    }

    public function setTclients(?Tclient $tclients): self
    {
        $this->tclients = $tclients;

        return $this;
    }
}
