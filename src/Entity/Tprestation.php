<?php

namespace App\Entity;

use App\Repository\TprestationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TprestationRepository::class)
 */
class Tprestation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codeprestation;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Isvendable = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Isfree = false ;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getCodeprestation(): ?string
    {
        return $this->codeprestation;
    }

    public function setCodeprestation(?string $codeprestation): self
    {
        $this->codeprestation = $codeprestation;

        return $this;
    }

    public function getIsvendable(): ?bool
    {
        return $this->Isvendable;
    }

    public function setIsvendable(?bool $Isvendable): self
    {
        $this->Isvendable = $Isvendable;

        return $this;
    }

    public function getIsfree(): ?bool
    {
        return $this->Isfree;
    }

    public function setIsfree(?bool $Isfree): self
    {
        $this->Isfree = $Isfree;

        return $this;
    }
}
